module gitlab.com/sujitbalasubramanian/chatgpt-api-chi

go 1.21.3

require (
	github.com/go-chi/chi v1.5.5
	github.com/go-chi/cors v1.2.1
	github.com/go-resty/resty/v2 v2.10.0
	github.com/joho/godotenv v1.5.1
)

require golang.org/x/net v0.17.0 // indirect
